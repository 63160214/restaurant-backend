import { Injectable } from '@nestjs/common';
import { CreateListorderDto } from './dto/create-listorder.dto';
import { UpdateListorderDto } from './dto/update-listorder.dto';

@Injectable()
export class ListordersService {
  create(createListorderDto: CreateListorderDto) {
    return 'This action adds a new listorder';
  }

  findAll() {
    return `This action returns all listorders`;
  }

  findOne(id: number) {
    return `This action returns a #${id} listorder`;
  }

  update(id: number, updateListorderDto: UpdateListorderDto) {
    return `This action updates a #${id} listorder`;
  }

  remove(id: number) {
    return `This action removes a #${id} listorder`;
  }
}
