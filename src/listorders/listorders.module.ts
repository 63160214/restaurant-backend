import { Module } from '@nestjs/common';
import { ListordersService } from './listorders.service';
import { ListordersController } from './listorders.controller';

@Module({
  controllers: [ListordersController],
  providers: [ListordersService],
})
export class ListordersModule {}
