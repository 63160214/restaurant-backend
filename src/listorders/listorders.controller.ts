import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ListordersService } from './listorders.service';
import { CreateListorderDto } from './dto/create-listorder.dto';
import { UpdateListorderDto } from './dto/update-listorder.dto';

@Controller('listorders')
export class ListordersController {
  constructor(private readonly listordersService: ListordersService) {}

  @Post()
  create(@Body() createListorderDto: CreateListorderDto) {
    return this.listordersService.create(createListorderDto);
  }

  @Get()
  findAll() {
    return this.listordersService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.listordersService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateListorderDto: UpdateListorderDto,
  ) {
    return this.listordersService.update(+id, updateListorderDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.listordersService.remove(+id);
  }
}
