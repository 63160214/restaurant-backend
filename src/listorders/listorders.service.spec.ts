import { Test, TestingModule } from '@nestjs/testing';
import { ListordersService } from './listorders.service';

describe('ListordersService', () => {
  let service: ListordersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ListordersService],
    }).compile();

    service = module.get<ListordersService>(ListordersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
