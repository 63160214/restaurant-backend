import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Menu } from './menus/entities/menu.entity';
import { Customer } from './customers/entities/customer.entity';
import { Product } from './products/entities/product.entity';
import { Order } from './orders/entities/order.entity';
import { OrderItem } from './order-items/entities/order-item.entity';
import { User } from './users/entities/user.entity';
import { Booking } from './booking/entities/booking.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'newgenesis_project',
      password: 'NewGenesis@2023',
      database: 'newgenesis_project',
      entities: [Customer, Product, Order, OrderItem, User, Booking, Menu],
      synchronize: true,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
