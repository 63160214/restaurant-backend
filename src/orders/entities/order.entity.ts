import { Booking } from 'src/booking/entities/booking.entity';
import { Customer } from 'src/customers/entities/customer.entity';
import { OrderItem } from 'src/order-items/entities/order-item.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
} from 'typeorm';

@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  amount: number;

  @Column({ type: 'float' })
  total: number;

  // @ManyToOne(() => Customer, (customer) => customer.orders)
  // customer: Customer;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;

  // @OneToOne(() => Booking, (booking) => booking.order)
  // booking: Booking;
  @OneToMany(() => OrderItem, (orderItem) => orderItem.menu)
  orderitem: OrderItem[];

  @OneToMany(() => OrderItem, (orderItem) => orderItem.order)
  orderItems: OrderItem[];
  // 1Order I-E OrderItem
  // OrderItem 3-I Order
}
