import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateOrderItemDto } from './dto/create-order-item.dto';
import { UpdateOrderItemDto } from './dto/update-order-item.dto';
import { OrderItem } from './entities/order-item.entity';

@Injectable()
export class OrderItemsService {
  constructor(
    @InjectRepository(OrderItem)
    private orderItemsRepository: Repository<OrderItem>,
  ) {}

  create(createOrderItemDto: CreateOrderItemDto) {
    return this.orderItemsRepository.save(createOrderItemDto);
  }

  findAll() {
    return `This action returns all orderItems`;
  }

  findOne(id: number) {
    return this.orderItemsRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateOrderItemDto: UpdateOrderItemDto) {
    try {
      const updatedOrderItem = await this.orderItemsRepository.save({
        id,
        ...updateOrderItemDto,
      });
      return updatedOrderItem;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const orderItem = await this.orderItemsRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedOrderItem = await this.orderItemsRepository.remove(
        orderItem,
      );
      return deletedOrderItem;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
