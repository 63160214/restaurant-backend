import { IsNotEmpty, Length, IsNumber, Min } from 'class-validator';

export class CreateOrderItemDto {
  @IsNotEmpty()
  amount: number;

  @IsNotEmpty()
  status: string;
}
