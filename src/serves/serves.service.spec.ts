import { Test, TestingModule } from '@nestjs/testing';
import { ServesService } from './serves.service';

describe('ServesService', () => {
  let service: ServesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ServesService],
    }).compile();

    service = module.get<ServesService>(ServesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
