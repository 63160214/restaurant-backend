import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ServesService } from './serves.service';
import { CreateServeDto } from './dto/create-serve.dto';
import { UpdateServeDto } from './dto/update-serve.dto';

@Controller('serves')
export class ServesController {
  constructor(private readonly servesService: ServesService) {}

  @Post()
  create(@Body() createServeDto: CreateServeDto) {
    return this.servesService.create(createServeDto);
  }

  @Get()
  findAll() {
    return this.servesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.servesService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateServeDto: UpdateServeDto) {
    return this.servesService.update(+id, updateServeDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.servesService.remove(+id);
  }
}
