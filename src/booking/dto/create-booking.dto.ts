import { IsNotEmpty, Length } from 'class-validator';

export class CreateBookingDto {
  @IsNotEmpty()
  table: string;

  @IsNotEmpty()
  status: string;

  date: string;

  time: string;

  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  @Length(10)
  phone: string;

  @IsNotEmpty()
  cusTotal: string;
}
