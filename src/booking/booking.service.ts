import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateBookingDto } from './dto/create-booking.dto';
import { UpdateBookingDto } from './dto/update-booking.dto';
import { Booking } from './entities/booking.entity';
import * as bcrypt from 'bcrypt';

@Injectable()
export class BookingService {
  constructor(
    @InjectRepository(Booking)
    private booksRepository: Repository<Booking>,
  ) {}

  create(createBookingDto: CreateBookingDto) {
    // const salt = await bcrypt.genSalt();
    // const hash = await bcrypt.hash(createBookingDto.password, salt);
    // createBookingDto.password = hash;
    return this.booksRepository.save(createBookingDto);
  }

  findAll(option) {
    return this.booksRepository.find(option);
  }

  findOne(id: number) {
    return this.booksRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateBookingDto: UpdateBookingDto) {
    try {
      // if (updateBookingDto.password !== undefined) {
      //   const salt = await bcrypt.genSalt();
      //   const hash = await bcrypt.hash(updateBookingDto.password, salt);
      //   updateBookingDto.password = hash;
      // }
      const updatedBooking = await this.booksRepository.save({
        id,
        ...updateBookingDto,
      });
      return updatedBooking;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const table = await this.booksRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedBooking = await this.booksRepository.remove(table);
      return deletedBooking;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
